const shell = require('shelljs'),
    path = require('path');

const basePath = process.cwd();

shell.ls('mutants').forEach(function (file) {
    const currentReportPath = path.join(basePath, 'mutants', file, 'report', 'vrt-report.html')
    console.log(currentReportPath)
    shell.sed('-i', /file:\/\/\//, '', currentReportPath)
    shell.sed('-i', /C:\\Users\\jtroconis\\Pruebas-Automatcias\\parcial-2\\scripts/, '../../..', currentReportPath)
})

