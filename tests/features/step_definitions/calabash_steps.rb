require 'calabash-android/calabash_steps'

Then /^I take a vrt screenshot at "([^\"]*)"$/ do |location|
    screenshot({:prefix => "./vrt/", :name => location})
end

Given /^I enter fillup date "([^\"]*)" "([^\"]*)" "([^\"]*)"$/ do |month, day, year|
    tap_mark("date")

    # Set Month
    touch("* marked:'numberpicker_input' index:0")
    clear_text("* marked:'numberpicker_input' index:0")
    enter_text("* marked:'numberpicker_input' index:0", month)

    # Set Day
    touch("* marked:'numberpicker_input' index:1")
    clear_text("* marked:'numberpicker_input' index:1")
    enter_text("* marked:'numberpicker_input' index:1", day)

    # Set Year
    touch("* marked:'numberpicker_input' index:2")
    clear_text("* marked:'numberpicker_input' index:2")
    enter_text("* marked:'numberpicker_input' index:2", year)

    tap_mark("OK")
end

# Press the idiotic button that goes against all good practices of navigation
Given /^I press the lower menu button$/ do 
    system("adb shell input tap 950 1850")
end

Then /Î press the lower menu butotn$/ do
    system("adb shell input tap 950 1850")
end

Then /^I hide the keyboard$/ do
    hide_soft_keyboard()
end