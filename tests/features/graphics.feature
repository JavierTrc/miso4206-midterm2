Feature: Graphic shows invalid licence

   Graphic shows invalid licence

   Scenario: Average fuel economy graphic
    Given I press "Statistics"
    And I press "Average fuel economy"
    Then I wait for the view with id "chart" to appear
    And I should see "Average fuel economy"

    Scenario: Worst fuel economy graphic
        Given I press "Statistics"
        And I press "Worst fuel economy"
        Then I wait for the view with id "chart" to appear
        And I should see "Worst fuel economy"

    Scenario: Best fuel economy graphic
        Given I press "Statistics"
        And I press "Best fuel economy"
        Then I wait for the view with id "chart" to appear
        And I should see "Best fuel economy"

    Scenario: Average distance graphic
        Given I press "Statistics"
        And I press "Average distance"
        Then I wait for the view with id "chart" to appear
        And I should see "Average distance between fillups"

    Scenario: Minimum distance graphic
        Given I press "Statistics"
        And I press "Minimum distance"
        Then I wait for the view with id "chart" to appear
        And I should see "Minimum distance between fillups"

    Scenario: Maximum distance graphic
        Given I press "Statistics"
        And I press "Maximum distance"
        Then I wait for the view with id "chart" to appear
        And I should see "Maximum distance between fillups"

    Scenario: Average cost graphic
        Given I press "Statistics"
        And I press "Average cost"
        Then I wait for the view with id "chart" to appear
        And I should see "Average cost per fillup"

    Scenario: Minimum cost graphic
        Given I press "Statistics"
        And I press "Minimum cost"
        Then I wait for the view with id "chart" to appear
        And I should see "Least expensive fillup"

    Scenario: Maximum cost graphic
        Given I press "Statistics"
        And I press "Maximum cost"
        Then I wait for the view with id "chart" to appear
        And I should see "Most expensive fillup"

    Scenario: Total cost graphic
        Given I press "Statistics"
        And I press "Total cost"
        Then I wait for the view with id "chart" to appear
        And I should see "Total amount spent on fuel"

    
    Scenario: Cost last month graphic
        Given I press "Statistics"
        And I press "Cost last month"
        Then I wait for the view with id "chart" to appear
        And I should see "Money spent in the last month"

    Scenario: Estimated cost per month graphic
        Given I press "Statistics"
        And I press "Estimated cost per month"
        Then I should see "No chart for this statistic (yet!)"

    Scenario: Cost last year graphic
        Given I press "Statistics"
        And I press "Cost last year"
        Then I wait for the view with id "chart" to appear
        And I should see "Money spent in the last year"

    Scenario: Average cost per mi graphic
        Given I press "Statistics"
        And I press "Average cost per mi"
        Then I should see "No chart for this statistic (yet!)"

    Scenario: Minimum cost per mi graphic
        Given I press "Statistics"
        And I press "Minimum cost per mi"
        Then I should see "No chart for this statistic (yet!)"

    Scenario: Maximum cost per mi graphic
        Given I press "Statistics"
        And I press "Maximum cost per mi"
        Then I should see "No chart for this statistic (yet!)"

    Scenario: Average fuel price graphic
        Given I press "Statistics"
        And I press "Average price"
        Then I wait for the view with id "chart" to appear
        And I should see "Average fuel price"

    Scenario: Minimum fuel price graphic
        Given I press "Statistics"
        And I press "Minimum price"
        Then I wait for the view with id "chart" to appear
        And I should see "Least expensive fuel"

    Scenario: Maximum fuel price graphic
        Given I press "Statistics"
        Then I scroll down
        And I press "Maximum price"
        Then I wait for the view with id "chart" to appear
        And I should see "Most expensive fuel"

    Scenario: Smallest fillup graphic
        Given I press "Statistics"
        Then I scroll down
        And I press "Smallest fillup"
        Then I wait for the view with id "chart" to appear
        And I should see "Smallest fillup size"

    Scenario: Largest fillup graphic
        Given I press "Statistics"
        Then I scroll down
        And I press "Largest fillup"
        Then I wait for the view with id "chart" to appear
        And I should see "Largest fillup size"

    Scenario: Average fillup graphic
        Given I press "Statistics"
        Then I scroll down
        And I press "Average fillup"
        Then I wait for the view with id "chart" to appear
        And I should see "Average fillup size"
    
    Scenario: Total purchased fuel graphic
        Given I press "Statistics"
        Then I scroll down
        And I press "Total fuel"
        Then I wait for the view with id "chart" to appear
        And I should see "Total fuel purchased"

    Scenario: Fuel per year graphic
        Given I press "Statistics"
        Then I scroll down
        And I press "Fuel per year"
        Then I should see "No chart for this statistic (yet!)"

    Scenario: Northest fillup graphic
        Given I press "Statistics"
        Then I scroll down
        And I press "North"
        Then I wait for the view with id "chart" to appear
        And I should see "Farthest north fillup"

    Scenario: Southest fillup graphic
        Given I press "Statistics"
        Then I scroll down
        And I press "South"
        Then I wait for the view with id "chart" to appear
        And I should see "Farthest south fillup"

    Scenario: Eastest fillup graphic
        Given I press "Statistics"
        Then I scroll down
        And I press "East"
        Then I wait for the view with id "chart" to appear
        And I should see "Farthest east fillup"

    Scenario: Westest fillup graphic
        Given I press "Statistics"
        Then I scroll down
        And I press "West"
        Then I wait for the view with id "chart" to appear
        And I should see "Farthest west fillup"