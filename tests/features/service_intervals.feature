Feature: Service Interval creation

   Service intervals are added correctly

    Scenario: Happy path, A service interval is added correctly
        Given I press the lower menu button
        And I press "Service intervals"
        Then I press "Add service interval"
        And I clear "title"
        And I clear "description"
        And I enter text "Random Service" into field with id "title"
        And I enter text "Random Desc" into field with id "description"
        And I hide the keyboard
        And I enter text "100" into field with id "odometer"
        And I press "Add service interval"
        Then I should see "Random Service"
        And I should see "Random Desc"

    Scenario: Empty title error
        Given I press the lower menu button
        And I press "Service intervals" 
        And I press the lower menu button 
        And I press "Add service interval"
        Then I clear "title"
        And I press "Add service interval"
        Then I should see "Invalid service interval title"

    Scenario: Empty odoemter error
        Given I press the lower menu button
        And I press "Service intervals" 
        And I press the lower menu button 
        And I press "Add service interval"
        And I wait to see "Service interval"
        And I hide the keyboard
        And I wait
        And I press "Add service interval"
        Then I should see "Invalid service interval odometer"

    Scenario: Empty description error
        Given I press the lower menu button
        And I press "Service intervals" 
        And I press the lower menu button 
        And I press "Add service interval"
        Then I clear "description"
        And I hide the keyboard
        And I enter text "100" into field with id "odometer"
        And I press "Add service interval"
        Then I should see "Invalid service interval description"

    Scenario: Edit service interval
        Given I press the lower menu button
        And I press "Service intervals"
        And I press "Random Service"
        Then I hide the keyboard
        And I wait
        And I press "Save changes"
        Then I wait to see "Service interval"
        And I should see "Oil change (standard)"
        And I should see "Standard oil change"

    Scenario: Delete service interval
        Given I press the lower menu button
        And I press "Service intervals"
        And I press "Oil change (standard)"
        And I wait
        Then I hide the keyboard
        And I press the lower menu button
        And I press "Delete"
        And I wait to see "Really delete?"
        Then I take a vrt screenshot at "Delete interval"
        Then I press "OK"
        And I wait to see "Service intervals"
        And I should not see "Oil change (standard)"