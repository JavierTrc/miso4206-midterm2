Feature: Fillup registration

   As a user I want to register a specific fillup

   Scenario: Invalid gallons error
        Then I press the "Save Fillup" button
        Then I should see "Invalid value for volume"

    Scenario: Invalid price error
        Then I enter text "100" into field with id "volume"
        Then I press the "Save Fillup" button
        Then I should see "Invalid value for price"

    Scenario: Invalid odometer error
        Then I enter text "100" into field with id "volume"
        And I enter text "100" into field with id "price"
        Then I press the "Save Fillup" button
        Then I should see "Invalid value for odometer"

   Scenario: Register a single fillup succesfully
        Given I enter text "1" into field with id "price"
        And I enter text "100" into field with id "volume"
        And I enter text "100" into field with id "odometer"
        And I enter fillup date "Jan" "01" "2018"
        Then I press the "Save Fillup" button
        Then I should see "100.00 g"
        And I should see "1.00"
        And I should see "1/1/18"
        Then I press "1/1/18"
        And I should see "false"
        And I should see "100.00 Gallons"
        And I should see "100.00 mi"
        And I should see "$1.00"
        And I should see "$100.00"

    Scenario: Register a second fillup succesfully
        Given I enter text "25" into field with id "price"
        And I enter text "100" into field with id "volume"
        And I enter text "200" into field with id "odometer"
        And I enter fillup date "Jan" "02" "2018"
        Then I press the "Save Fillup" button
        Then I should see "100.00 g"
        And I should see "25.00"
        And I should see "1/2/18"
        And I should see "1.00 mpg"
        Then I press "1/2/18"
        And I should see "false"
        And I should see "100.00 Gallons"
        And I should see "200.00 mi"
        And I should see "$25.00"
        And I should see "$2500.00"

    Scenario: Statistics are updated correctly after a second fillup
        Given I press "Statistics"
        Then I should see "1.00 mpg"
        And I should see "100.00 mi"
        And I should see "$1300.00"
        And I should see "$100.00"
        And I should see "$2500.00"
        And I should see "$2600.00"
        And I should see "$0.00"
        And I should see "$39000.00 / mo"
        And I should see "$474500.00 / yr"
        And I should see "$25.00 / mi"
        And I should see "$13.00"
        And I should see "$1.00"
        Then I scroll down
        And I should see "$25.00"
        And I should see "100.00 Gallons"
        And I should see "200.00 Gallons"
        And I should see "36500.00 Gallons / yr"

    Scenario: Add a partial fillup
        Given I enter text "50" into field with id "price"
        And I enter text "50" into field with id "volume"
        And I enter text "225" into field with id "odometer"
        And I enter fillup date "Jan" "03" "2018"
        Then I toggle checkbox number 1
        Then I press the "Save Fillup" button
        Then I should see "50.00 g"
        And I should see "50.00"
        And I should see "1/2/18"
        And I should see "partial"
        Then I press "1/3/18"
        And I should see "true"

    Scenario: Statistics are updated correctly after a partial fillup
        Given I press "Statistics"
        Then I should see "1.00 mpg"
        And I should see "50.00 mi"
        And I should see "25.00 mi"
        And I should see "100.00 mi"
        And I should see "$1700.00"
        And I should see "$100.00"
        And I should see "$2500.00"
        And I should see "$5100.00"
        And I should see "$0.00"
        And I should see "$51000.00 / mo"
        And I should see "$5000.00"
        And I should see "$620500.00 / yr"
        And I should see "$50.00 / mi"
        And I should see "$25.00 / mi"
        And I should see "$100.00 / mi"
        And I should see "$25.33"
        And I should see "$1.00"
        Then I scroll down
        And I should see "$50.00"
        And I should see "50.00 Gallons"
        And I should see "100.00 Gallons"
        And I should see "83.33 Gallons"
        And I should see "250.00 Gallons"
        And I should see "24333.33 Gallons / yr"