require 'calabash-android/cucumber'
require 'fileutils'

dir = File.expand_path("../../vrt",__dir__)
FileUtils.rm_rf dir
FileUtils.mkdir dir

dir = File.expand_path("../../screenshots",__dir__)
FileUtils.rm_rf dir
FileUtils.mkdir dir
