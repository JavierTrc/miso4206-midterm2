Feature: Navigation

  Scenario: As a user I want to navigate to the Statistics Activity
    Given I press "Statistics"
    Then I wait to see "Fuel economy"
    Then I take a vrt screenshot at "Statistics upper"             
    And I should see "Average fuel economy"
    And I should see "Worst fuel economy"
    And I should see "Best fuel economy"
    And I should see "Distance between fillups"
    And I should see "Average distance"
    And I should see "Minimum distance"
    And I should see "Maximum distance"
    And I should see "Fillup costs"
    And I should see "Average cost"
    And I should see "Minimum cost"
    Then I scroll down
    And I wait to see "Maximum cost"
    Then I take a vrt screenshot at "Statistics lower"
    And I should see "Total cost"
    And I should see "Cost last month"
    And I should see "Estimated cost per month"
    And I should see "Cost last year"
    And I should see "Estimated cost per year"
    And I should see "Cost per distance"
    And I should see "Average cost per mi"
    And I should see "Minimum cost per mi"
    And I should see "Maximum cost per mi"
    And I should see "Average cost per mi"
    And I should see "Fuel price"
    And I should see "Average price"
    And I should see "Minimum price"
    And I should see "Maximum price"
    And I should see "Fuel consumption"
    And I should see "Smallest fillup"
    And I should see "Largest fillup"
    And I should see "Average fillup"
    And I should see "Total fuel"
    And I should see "Fuel per year"
    And I should see "Fillup locations"
    And I should see "North"
    And I should see "South"
    And I should see "East"
    And I should see "West"

  Scenario: As an user I want to navigate to the vehicles activity
    Given I press "Vehicles"
    Then I wait to see "Default vehicle"
    Then I take a vrt screenshot at "Vehicles default"
    And I should see "Auto-generated vehicle"

  Scenario: As an user I want to navigate to the add vehicles activity
    Given I press "Vehicles"
    Then I press "Default vehicle"
    And I wait to see "Vehicle information"
    And I hide the keyboard
    Then I take a vrt screenshot at "Add Vehicle"

  Scenario: As an user I want to navigate to the fillup activity
    Given I press "Fillup"
    Then I wait to see "Fillup information"
    Then I take a vrt screenshot at "Fillup"
    And I should see "Tank was not filled to the top"
    And I should see "Extra data"
    And I should see "Save Fillup"

  Scenario: As an user I want to navigate to the settings screen
    Given I press the lower menu button
    And I press "Settings"
    Then I should see "Settings"
    And I take a vrt screenshot at "Settings"

  Scenario: As an user I want to navigate to the service intervals screen
    Given I press the lower menu button
    And I press "Service intervals"
    Then I should see "Service intervals"
    And I take a vrt screenshot at "Service intervals"

  Scenario: As an user I want to navigate to the add service intervals screen
    Given I press the lower menu button
    And I press "Service intervals"
    And I press "Add service interval"
    Then I wait to see "Template to use (optional)"
    And I hide the keyboard
    And I take a vrt screenshot at "Add service interval"

  Scenario: As an user I want to navigate to the import/export activity
    Given I press the lower menu button
    And I press "Import / Export"
    Then I should see "Import / Export data"
    And I take a vrt screenshot at "Export-Import data"
