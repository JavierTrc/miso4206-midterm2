const shell = require('shelljs'),
    path = require('path'),
    fs = require('fs');

const mutantPrep = require('./utils/mutatantPrep'),
    bddUtils = require('./utils/bddTestsUtils'),
    vrtutils = require('./utils/vrtUtils'),
    emulatorUtils = require('./utils/emulatorUtils'),
    adbUtils = require('./utils/adbUtils'),
    reportUtils = require('./utils/reportUtils');

const workingDir = path.normalize(process.cwd()) 
    testsPath = path.normalize(`${process.cwd()}/tests`),
    mutantsPath = path.normalize(`${process.cwd()}/mutants`),
    vrtBaseline = path.normalize(`${process.cwd()}/baseline/vrt`);

const apkName = 'com.evancharlton.mileage_3110',
    avdName = 'Nexus_5x_API_27';

async function processMutants() {
    const failedBddMutants = [];
    const serial = emulatorUtils.runEmulator(avdName).serial;
    process.env.ADB_DEVICE_ARG = serial;
    process.env.SCREENSHOT_PATH = path.join(testsPath, 'screenshots/');
    await adbUtils.waitForEmulator();
    await asyncForEach(shell.ls(mutantsPath), async (file) => {
        console.log(`Mutant ${file}`)
        const currentMutantPath = path.normalize(`${mutantsPath}/${file}`)
        // Pre process APK to make sure that it has the internet permission
        shell.cd(currentMutantPath);
        await mutantPrep.prepMutant(apkName);
        shell.cd(workingDir);

        console.log(`Finished prepping, starting bdd`)
        // Copy apk to test folder
        const bddReportPath = path.join(currentMutantPath, 'report.html');
        if(!shell.test('-f', bddReportPath)) {
            console.log('starting bdd')
            shell.cd(testsPath);
            const bddResult = await bddUtils.runBDD(testsPath, currentMutantPath, apkName + '.apk');
            shell.cp('-rf', path.join(testsPath, 'report.html'), currentMutantPath);
            shell.cp('-rf', path.join(testsPath, 'screenshots/'), currentMutantPath);
            shell.cd(workingDir);
            if(bddResult) {
                failedBddMutants.push(bddResult)
            }
        }

        console.log(`Finished bdd, starting vrt`)
        prepVrtDir(currentMutantPath);
        await vrtutils.performVrt(vrtBaseline, currentMutantPath, file);

    })
    emulatorUtils.stopEmulator(avdName);
    reportUtils.writeReport(workingDir, failedBddMutants);
}

function prepVrtDir(currentMutantPath) {
    const newVrtResults = path.normalize(`${testsPath}/vrt`);

    shell.cp('-rf', newVrtResults, currentMutantPath);
    const mutantVrt = path.normalize(`${currentMutantPath}/vrt`)
    shell.ls(mutantVrt).forEach(file => {
        const newFileName = file.replace(/_\d+\.png$/, '.png');
        const currentImagePath = path.normalize(`${mutantVrt}/${file}`);
        const newImagePath = path.normalize(`${mutantVrt}/${newFileName}`);
        fs.renameSync(currentImagePath, newImagePath);
    })
}

async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
      await callback(array[index], index, array)
    }
}

processMutants()