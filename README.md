# Parcial 2 Pruebas Automáticas

La estrategia de pruebas escogida para realizar las pruebas de las apps se basó en el uso de escenarios BDD para definir un subconjunto de las funcionalidades para poder analizar y realizar la regresión sobre estas. BDD fue escogido porque permitió agregar pasos para tomar los screenshots y realizar VRT con un conjunto de imágenes en los mismos momentos durante las pruebas. Usando este conjunto de pruebas se realizó el mismo procedimiento para cada mutante para intentar identificar los errores en cada mutante. Dentro de la carpeta mutante se muestra los mutantes con los reportes generados para cada uno usando el procedimiento mencionado. 

## Mutantes No Probados

Las pruebas no se pudieron hacer sobre todos los mutantes. Por cada mutante las pruebas pueden duran aproximadamente 7 minutos entre los escenarios de BDD y el proceso de generación del reporte de VRT. Se tuvo problemas con la ejecución continuada de las pruebas y se tuvo que volver a realizar desde el comienzo la ejecución de todas las pruebas. Se logró realizar el reporte de 455 mutantes. Estos se encuentran en la carpeta mutants; los mutantes no probados son los que se encuentran en non-tested-mutants. Los mutantes se escogieron de forma aleatoria y la limitación se encontró en el tiempo tomando en cuenta el tiempo perdido por las fallas iniciales.

## Presupuesto para pruebas

Se contó con un presupuesto de horas máquina de 60 horas y de horas persona de 12 horas. La combinación de los largos tiempos de ejecución y la gran cantidad de mutantes difucultó la generación de un reporte único. Aunque la naturaleza de BDT igual falicitó realizar la traza de los errores debido a la definición de los pasos.

## Ejecución de las pruebas

Para poder ejecutar las pruebas el equipo donde se vaya a ejecutar debe cumplir con las siguientes características:

- Tener instalado NodeJs
- Contar con el Android SDK (para el lanzamiento del emulador)
- Tener instalado Ruby y contar con la gema de Bundle

Las pruebas se ejecutan usando:

`node app.js`

Se necesitan eliminar los siguientes archivos para que se realicen las pruebas de nuevo:

- mutants/\<mutant-name\>/report.html
- mutants/\<mutant-name\>/report/vrt-report.html

Los scripts poseen guardas que no corren las pruebas si ya se encuentran generados para facilitar la ejecución y no tener que comenzar de 0 en caso de que se interrumpa la ejecución en algún momento.

## Visualizacion de los resultados

Lor reportes generados se encuentran en las rutas mencionados al final de la sección anterior. El primero:

`mutants/<mutant-name>/report.html`

Posee los pasos de calabash en formato HTML con el momento en que hubo un error (si lo hubo). Se utilizó un esquema de fail fast para intentar reducir los tiempos de cada ejecución y procesar un mayor número de mutantes. En los pasos se encuentran los pasos de vrt incluidos para realizar la toma de imagenes para realizar el procesamiento de VRT posterior. 

En la ruta:

`mutants/<mutant-name>/report/vrt-report.html`

Estan los reportes de vrt disponibles. Estos poseen una comparacion de las imagenes tomadas en el baseline con las tomadas por las mismas pruebas en la versión mutada. Se encuentran solo las comparaciones de las imagenes que se encuentran en las 2 versiones. Se definieron hooks y steps de calabash especializados para realizar la toma de datos necesarios y limpiar las carpetas de screenshots debido a las limitaciones que tiene calabash para la toma de screenshots. Estos despues se copiaron en la carpeta del mutante para persistir el reporte generado.