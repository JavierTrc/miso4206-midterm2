const shell = require('shelljs'),
    xml2js = require('xml2js'),
    path = require('path');

const processedPath = 'processed.json'

const public = {};

public.prepMutant = async (apkName) => {
    if (shell.test('-f', processedPath)) return;
    await execPromise(`apktool d ${apkName}.apk -f`);
    const manifestFilePath = path.normalize(`${apkName}/AndroidManifest.xml`);

    //Add internet permission to APK
    const manifest = await xml2jsPromise(shell.cat(manifestFilePath));
    manifest['manifest']['uses-permission'].push({
        '$': {'android:name':'android.permission.INTERNET'}
    })
    const builder = new xml2js.Builder({xmldec: { encoding: 'utf-8', standalone: false }});
    const newManifest =  builder.buildObject(manifest);
    shell.echo(newManifest).to(manifestFilePath)
    await execPromise(`apktool b ${apkName} -o ${apkName}.apk`)
    shell.echo(JSON.stringify({processed: true})).to(processedPath)
    shell.rm('-rf', apkName)
}

function execPromise(command) {
    return new Promise((resolve, reject) =>{
        shell.exec(command, (code, stdout, stderr) => {
            if(code === 0) {
                resolve({code, stdout, stderr})
            } else {
                reject(Error(`${command} failed with code ${code}`))
            }
        })
    }) 
}

function xml2jsPromise(xmlStr) {
    return new Promise((resolve, reject) => { 
        xml2js.parseString(xmlStr, (err, result) =>{
            if(err) {
                reject(err)
            } else {
                resolve(result)
            }
        })
    })
}

module.exports = public