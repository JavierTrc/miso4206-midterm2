const resemble = require("node-resemble-js"),
    shell = require('shelljs'),
    path = require('path')
    fs = require('mz/fs');

const public = {};

const vrtOptions = {
    output: {
        errorColor: {
            red: 255,
            green: 0,
            blue: 255
        },
        errorType: "movement",
        transparency: 0.3,
        largeImageThreshold: 1200,
        useCrossOrigin: false,
        outputDiff: true
    },
    scaleToSameSize: true,
    ignore: "antialiasing"
};

public.performVrt = async (baselinePath, currentVersionPath, mutantName) => {
    if(!shell.test('-d', baselinePath)) return;
    if(!shell.test('-d', currentVersionPath)) return;
    if(shell.test('-f', `${currentVersionPath}/report/vrt-report.html`)) return;

    const diffPath = path.normalize(`${currentVersionPath}/report/diffs`)
    if(!shell.test('-d', diffPath)) shell.mkdir('-p', diffPath);

    var newLine = '\n';
    var html = `<h1>VRT Report versions Baseline & ${mutantName}</h1>` + newLine;
    html+='<br><table>' + newLine;
    html+=`<tr><th>Versión Baseline</th><th>Versión ${mutantName}</th><th>Diferences</th><th>Important information</th></tr>` + newLine;
    
    await asyncForEach(shell.ls(baselinePath), async (file) => {
        const baselinePic = path.normalize(`${baselinePath}/${file}`)
        const mutantPic = path.normalize(`${currentVersionPath}/vrt/${file}`)

        baselinePicBuff = await fs.readFile(baselinePic)
        mutantPicBuff = await fs.readFile(mutantPic)

        const data = await compareImageAsync(baselinePicBuff, mutantPicBuff)

        const diffImgPath = path.normalize(`${diffPath}/${file}`)
        await fs.writeFile(diffImgPath, data.getDiffImageAsJPEG());
        text = data;
        html+='<tr>' + newLine;
        html+=`<td><img src="file:///${baselinePic}" style="height: 380px; width: 216px"></td>` + newLine;
        html+=`<td><img src="file:///${mutantPic}" style="height: 380px; width: 216px"></td>` + newLine;
        html+='<td><img src="'+diffImgPath+'" style="height: 380px; width: 216px"></td>' + newLine;
        html+=`<td><h4>Mutant: ${mutantName}<br> location: ${file}<br> misMatchPercentage: `+text.misMatchPercentage+'<br>isSameDimensions:'+text.isSameDimensions+'<br>dimensionDifference:{width:'+text.dimensionDifference.width+', height:'+text.dimensionDifference.height+'}</h4></td>' + newLine;
        html+='</tr>' + newLine;
    })

    html+='</table>';
    if(!shell.test('-d', `${currentVersionPath}/report`)) shell.mkdir('-p', dir);
    await fs.writeFile(`${currentVersionPath}/report/vrt-report.html`, html);
}

async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
      await callback(array[index], index, array)
    }
}

function compareImageAsync(file1, file2) {
    return new Promise((resolve, reject) => {
            resemble(file1).compareTo(file2).ignoreAntialiasing().onComplete(data => {
                resolve(data)
            })
    })
}

module.exports = public;