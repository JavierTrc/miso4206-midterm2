const shell = require('shelljs'),
    path = require('path');

const public = {};

public.runBDD = async (testsPath, apkPath, apkName) => {
    const fullApkPath = path.normalize(`${apkPath}/${apkName}`)
    shell.cp(fullApkPath, testsPath)
    await execPromise(`bundle exec calabash-android resign ${apkName}`)
    try {
        await execPromise(`bundle exec calabash-android run ${apkName} --format html --out report.html --fail-fast`)
        return undefined;   
    } catch (error) {
        return error.stderr;
    }
}

function execPromise(command) {
    return new Promise((resolve, reject) =>{
        shell.exec(command, (code, stdout, stderr) => {
            if(code === 0) {
                resolve({code, stdout, stderr})
            } else {
                const err = Error(`${command} failed with code ${code}`)
                err.stderr = stderr
                reject(err)
            }
        })
    }) 
}

module.exports = public;