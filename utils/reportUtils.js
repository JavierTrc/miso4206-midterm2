const path = require('path');
const fs = require('fs');


const public = {}

public.writeReport = (dir, report) => {

    const fileName  = `report.json`;
    const reportPath = path.join(dir, fileName);

    fs.writeFile(reportPath, JSON.stringify(report), function(err) {
        if(err) {
            console.log(err);
        }
        console.log(reportPath + " was saved!");
    });
}

module.exports = public